package com.cucumber.pageactions;

import com.cucumber.utils.BasePage;

import static com.cucumber.pageobjects.HomepagePO.BTN_CREATE_CAMPAIGN;
import static com.cucumber.pageobjects.LoginPO.*;
import static com.cucumber.utils.CommonFunctions.*;

public class Homepage extends BasePage {

    public void clickCreateCampaign() throws Exception {
        handleIframe(BTN_CREATE_CAMPAIGN);
        click(BTN_CREATE_CAMPAIGN);
    }
    
    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad(FORM_LOGIN);
    }
}
