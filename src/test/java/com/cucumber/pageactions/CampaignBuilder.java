package com.cucumber.pageactions;

import com.cucumber.model.CampaignType;
import com.cucumber.utils.BasePage;

import static com.cucumber.pageobjects.CampaignBuilderPO.*;
import static com.cucumber.pageobjects.HomepagePO.*;
import static com.cucumber.utils.CommonFunctions.*;

public class CampaignBuilder extends BasePage {

    public void clickCreateCampaign() {
        click(BTN_CREATE_CAMPAIGN);
        waitPageToLoad();
    }

    public void selectCampaignType(CampaignType campaignType) {
        switch (campaignType) {
            case INFLUENCER:
                click(BTN_CREATE_INFLUENCER_CAMPAIGN);
                break;
            case CONTENT:
                click(BTN_CREATE_CONTENT_CAMPAIGN);
                break;
        }
    }

    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad();
    }
}
