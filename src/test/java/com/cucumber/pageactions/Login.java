package com.cucumber.pageactions;

import org.junit.Assert;
import com.cucumber.utils.BasePage;

import java.util.Locale;

import static com.cucumber.pageobjects.HomepagePO.*;
import static com.cucumber.pageobjects.LoginPO.*;
import static com.cucumber.utils.CommonFunctions.*;

public class Login extends BasePage {

    public void enterEmail(String str) {
        waitPageToLoad();
        type(str, TXT_EMAIL_LOGIN);
//        txtEmailLogin.sendKeys(str);
    }

    public void enterPassword(String password){
        type(password, TXT_PASSWORD_LOGIN);
    }

    public void clickLogin() {
        click(BTN_LOGIN);
    }

    public void verifyLoginSuccessful() {
        Assert.assertTrue(isElementVisible(HOMEPAGE_HEADER));
    }

    public void verifyElementIsDisplayed(String element) {
        switch (element.toUpperCase()) {
            case "USERNAMETEXTFIELD":
                System.out.println(element);
                waitPageToLoad(IMG_LOGO);
                Assert.assertTrue(isElementVisible(IMG_LOGO));
                break;
            case "PASSWORDTEXTFIELD":
                System.out.println(element);
                waitPageToLoad(TXT_SEARCH);
                Assert.assertTrue(isElementVisible(TXT_SEARCH));
                break;
        }
    }

    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad(FORM_LOGIN);
    }
}
