package com.cucumber.pageactions;

import com.cucumber.utils.BasePage;
import static org.aspectj.bridge.MessageUtil.info;

public class Google extends BasePage {

    public void enterSearchKeyword(String keyword) {
        info("Enter Search Keyword");
    }

    public void hitEnterKey() {
        info("Hit Enter Key");
    }

    public void verifySearchResultPage() {
        info("Verify Search Result Page");
    }
}
