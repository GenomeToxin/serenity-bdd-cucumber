package com.cucumber.model;

public enum CampaignType {
    INFLUENCER,
    CONTENT
}
