package com.cucumber.steps;

import com.cucumber.pageactions.Login;
import com.cucumber.utils.DriverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class LoginSteps {

    Login login;

    public LoginSteps() {
        this.login = new Login();
    }

    @Given("User enters email {string}")
    public void enterEmail(String email) {
        login.enterEmail(email);
    }

    @Given("User enters password {string}")
    public void enterPassword(String password) {
        login.enterPassword(password);
    }

    @Given("User clicks login button")
    public void clickLogin() {
        login.clickLogin();
    }

    @Then("User shall login successfully")
    public void verifyLoginSuccessful() {
        login.verifyLoginSuccessful();
    }

    @Given("Verify {string} is displayed")
    public void verifyElementIsDisplayed(String element) {
        login.verifyElementIsDisplayed(element);
    }

    @Then("Tear Down")
    public void tearDown() {
        DriverManager.removeDriver();
    }
}
