package com.cucumber.steps;

import com.cucumber.pageactions.Google;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GoogleSteps {

    Google google = new Google();

    @Given("User enters search keyword {string}")
    public void userEntersSearchKeyword(String keyword) {
        google.enterSearchKeyword(keyword);
    }

    @When("User hit enter key")
    public void userHitEnterKey() {
        google.hitEnterKey();
    }

    @Then("User shall be redirected to search results page")
    public void userShallBeRedirectedToSearchResultsPage() {
        google.verifySearchResultPage();
    }
}
