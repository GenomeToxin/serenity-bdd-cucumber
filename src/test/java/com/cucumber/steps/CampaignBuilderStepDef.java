package com.cucumber.steps;

import com.cucumber.pageactions.CampaignBuilder;
import com.cucumber.pageactions.Homepage;
import io.cucumber.java.en.Given;
import com.cucumber.model.CampaignType;

public class CampaignBuilderStepDef {

    CampaignBuilder campaignBuilder;
    Homepage homepage;

    public CampaignBuilderStepDef() {
        this.campaignBuilder = new CampaignBuilder();
        this.homepage = new Homepage();
    }

    @Given("User click Create Campaign")
    public void clickCreateCampaign() throws Exception {
        homepage.clickCreateCampaign();
    }

    @Given("User creates an {campaignType} Campaign")
    public void selectCampaignType(String campaignType) {
        CampaignType campaign = null;
        switch (campaignType.toUpperCase()) {
            case "INFLUENCER":
                campaign = CampaignType.INFLUENCER;
                break;
            case "CONTENT":
                campaign = CampaignType.CONTENT;
                break;
        }
        campaignBuilder.selectCampaignType(campaign);
    }
}
