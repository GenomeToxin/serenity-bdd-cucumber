package com.cucumber.steps;

import com.cucumber.pageactions.Login;

public class ParameterType {

    Login login;

    public ParameterType() {
    }

    @io.cucumber.java.ParameterType("Influencer|Content")
    public static String campaignType(String campaignType) {
        return campaignType;
    }
}
