package com.cucumber.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class HomepagePO extends PageObject {
    public static final By HOMEPAGE_HEADER = xpath("//*[@data-cy-element='title-bar']");
    public static final By BTN_CREATE_CAMPAIGN = xpath("//*[@data-cy-element='create-campaign-button']");
}
