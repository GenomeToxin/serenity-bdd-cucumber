package com.cucumber.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class InfluencerDetailsPO extends PageObject {
    public static final By BTN_UPLOAD_LOGO = xpath("//*[@data-testid='empty-moodboard-image']");
    public static final By INPUT_UPLOAD_LOGO = xpath("//*[@type='file']");
    public static final By TXT_BRAND_NAME = xpath("//*[@name='name']");
    public static final By ICO_ADD_BRAND_LOGO = xpath("//*[@data-cy-element='save-brand-logo']//*[contains(@class,'action-icon-wrapper')]");
    public static final By ICO_ADD_BRAND_LOGO_DISABLED = xpath("//*[@data-cy-element='save-brand-logo']//*[contains(@class,'action-icon-wrapper')][@disabled]");

    public static final By LIST_CAMPAIGN = xpath("//header/following-sibling::div[2]//a");
}
