package com.cucumber.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class LoginPO extends PageObject {
    public static final By FORM_LOGIN = xpath("//*[@name='sign-in']");
    public static final By TXT_EMAIL_LOGIN = xpath("//*[@name='email']");
    public static final By TXT_PASSWORD_LOGIN = xpath("//*[@name='password']");
    public static final By BTN_LOGIN = xpath("//*[@data-cy-element='log-in-button']//span");

    @FindBy(xpath = "//*[@name='email']")
    public static WebElementFacade txtEmailLogin;

    public static final By IMG_LOGO = xpath("//*[@alt='Google']");
    public static final By TXT_SEARCH = xpath("//*[@title='Search']");
}
