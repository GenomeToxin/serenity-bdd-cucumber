package com.cucumber.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class CampaignBuilderPO extends PageObject {
    public static final By BTN_CREATE_INFLUENCER_CAMPAIGN = xpath("//*[@data-cy-element='social_submission-campaign-button']");
    public static final By BTN_CREATE_CONTENT_CAMPAIGN = xpath("//*[@data-cy-element='content_submission-campaign-button']");
}
