package com.cucumber.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileReader;
import java.io.Reader;
import java.util.Properties;


public class DriverManager {
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

    public static synchronized WebDriver getDriver() {
        return webDriver.get();
    }

    public static synchronized WebDriver setDriver(String browser) throws Exception {
        WebDriver driver = null;
        Reader reader = new FileReader("src/test/java/config.properties");
        Properties properties = new Properties();
        properties.load(reader);

        switch (browser) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                FirefoxOptions ffOptions = new FirefoxOptions().addPreference("browser.startup.page", 1);
                FirefoxBinary firefoxBinary = new FirefoxBinary();
                ffOptions.setBinary(firefoxBinary);
                driver = new FirefoxDriver(ffOptions);
                break;
            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--whitelisted-ips");
                chromeOptions.addArguments("--disable-gpu");
                chromeOptions.addArguments("--disable-dev-shm-usage");
                chromeOptions.addArguments("--disable-infobars");
                chromeOptions.addArguments("--disable-extensions");
                chromeOptions.addArguments("--disable-plugins");
                chromeOptions.addArguments("--disable-popup-blocking");
                chromeOptions.addArguments("incognito");
                chromeOptions.addArguments("start-maximized");
                chromeOptions.addArguments("--disable-browser-side-navigation");
                chromeOptions.addArguments("--log-level=2");

                if (properties.getProperty("headless").equalsIgnoreCase("yes")) {
                    chromeOptions.addArguments("--headless");
                }

                chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

                chromeOptions.setPageLoadStrategy(PageLoadStrategy.NONE);
                driver = new ChromeDriver(chromeOptions);

//                System.setProperty("webdriver.chrome.verboseLogging", "true");
                break;
        }

        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        System.out.println("Running in " + browser + " version: " + cap.getVersion());

        return driver;
    }

    public static synchronized void setWebDriver(String browser, String url) throws Exception {
        webDriver.set(setDriver(browser));
        getDriver().navigate().to(url);
    }

    public static synchronized void removeDriver() throws NoSuchSessionException {
        try {
            if (getDriver() !=null) {
                getDriver().manage().deleteAllCookies();
//                getDriver().close();
                getDriver().quit();
                webDriver.remove();
            }
        } catch(Exception e) {
            e.getStackTrace();
        }

    }

}
