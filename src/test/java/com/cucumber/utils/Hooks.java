package com.cucumber.utils;

import io.cucumber.java.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.WebdriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Properties;

import static com.cucumber.utils.DriverManager.getDriver;
import static org.aspectj.bridge.MessageUtil.info;

public class Hooks {
    FileReader reader = new FileReader("src/test/java/config.properties");
    Properties properties = new Properties();

    public Hooks() throws FileNotFoundException {
    }

    @Before(order=1)
    public void beforeScenario() throws Exception {
        properties.load(reader);
        DriverManager.setWebDriver((String) properties.get("browser"), properties.getProperty("url"));
        System.out.println(getDriver());
        System.out.println("Open browser");
        info("Start the browser and Clear the cookies");
    }
    @Before(order=0)
    public void beforeScenarioStart(){
        System.out.println("-----------------Start of Scenario-----------------");
    }

    @After(order=0)
    public void afterScenarioFinish(){
        System.out.println("-----------------End of Scenario-----------------");
    }

    /*@After(order=1)
    public void afterScenario(){
        System.out.println(getDriver());
        DriverManager.removeDriver();
        info("Close the browser");
    }*/
}
