package pipeline

pipeline {
    agent {
        node {
            label 'master'
        }
    }

    options {
        timestamps()
    }

    stages {
        stage ("Checkout From Repository") {
            steps{
                git 'https://GenomeToxin@bitbucket.org/GenomeToxin/serenity-bdd-cucumber.git'
            }
        }

        stage ("Build Tests") {
            steps {
                bat(/mvn clean install/)
            }
        }

        stage ("Execute Tests") {
            steps {
                bat(/mvn test verify/)
            }
        }
    }
}